import ClassProject, sqlite3

class TaskManager():

	# Attribute:
		# Projects = list: Stores all projects in real time.
		# DB = sqlite3.connect: Connection with the data base.

	def __init__(self):
		self.Projects = []
		self.DB = sqlite3.connect("DataBase.db")
	
	def newProject(self, name):
		self.Projects.append(ClassProject.Project(name))

	def save(self):
		pass		


if __name__ == '__main__':
	TM = TaskManager()
	TM.DB.cursor().execute("SELECT name FROM sqlite_master")
	print(TM.DB.cursor().fetchall())