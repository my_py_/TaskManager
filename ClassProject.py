import ClassTask, json

class Project():

	# Attribute:
		# Name = Strng: Project name.
		# Tasks = list: Array for storing tasks.
		# Patch = String: Project patch.
		# GIT = Class:Git.Repo: сделай сам, ссылка на удалённый репозиторий.
		# 
	def __init__(self, name, patch):
		self.Tasks = [] 
		self.Name = name 
		self.Patch = patch # Идти в сохранение не должно, так как у каждого локально может быть в разных местах

#
	def __repr__(self):
		return f"{self.Name}"

	def newTask(self, name):
		self.Tasks.append(ClassTask.Task(name))

	def Save(self, action='save'):
		if action != 'save' and action != 'return': raise ValueError(f"Invalid value by a attribute 'action'.\n")
		listTasks = []
		for task in self.Tasks:
			listTasks.append(task.getForSave())
		data = { "Name":self.Name, "Tasks":listTasks }
		if action == 'return': return data
		# Save data:

if __name__ == '__main__':
	P = Project('Tasls')
	P.Save(action='return')