import json

class Task:

	# Attribute:
		# Name = Strng: Task name.
		# Tasks = list: Array for storing tasks.
		# Completed = bool: Task completed.
	
	def __init__(self, name):
		self.Name = name 
		self.Tasks = []
		self.Completed = False

	def __repr__(self):
		return f"{self.Name}"

	def newTask(self, name):
		self.Tasks.append(Task(name))

	def setCompleted(self):
		self.Completed = True
		for task in self.Tasks:
			task.Completed = True

	def getForSave(self):
		listTasks = []
		for task in self.Tasks:
			listTasks.append(task.getForSave())
		data = { "Name":self.Name, "Tasks":listTasks }
		return data