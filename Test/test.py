import sys, os
from random import randint
sys.path.append('//home/Code/Python/TaskManager')

from main import TaskManager

PM = TaskManager()
Up = PM
os.system('clear')

def CreateTasks():
	for P in range(1, 6):
		PM.newProject(f'Project{P}')
		#print(f"create: Project{P}")
		for T in range(1,randint(2,4)):
			PM.Projects[P-1].newTask(f'Task{T}')
			#print(f"create: Task{T}")
			if randint(1,10) < 8:
				for t in range(1,randint(2,8)):
					PM.Projects[P-1].Tasks[T-1].newTask(f'miniTask{t}')

def test1():
	print("\nt\t\tHI")

	for P in PM.Projects:
		print(f'|{P} {len(P.Tasks)}')
		for T in P.Tasks:
			print(f'|---{T} {len(T.Tasks)}')
			for t in T.Tasks:
				print(f'|------{t}')

def test2():
	data = PM.save()
	s = ''
	for i in data:
		s += i.get('Name') + '\n'
		for T in i.get('Tasks'):
			s += '\t'+T.get('Name') + '\n'
			for t in T.get('Tasks'):
				s += '\t\t'+t.get('Name') + '\n'



	print(s)


CreateTasks()
test2()